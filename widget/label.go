package widget

import (
	//"fmt"

	"gitlab.com/leonsal/gowebpanel/core"
	"gitlab.com/leonsal/gowebpanel/dom"
	"gitlab.com/leonsal/gowebpanel/theme"
)

// Label is a panel which contains a text.
// The content size of the label panel is the exact size of the text.
type Label struct {
	core.Panel            // embedded Panel
	font       core.Font  // current font
	color      core.Color // current text color
	text       string     // text being displayed
}

func NewLabel(text string) *Label {

	theme := theme.Current()
	l := new(Label)
	l.Panel.Init(0, 0)
	l.text = text
	l.font = theme.Font
	l.color = theme.ColorNormalText
	l.SetPaddingFrom(&theme.LabelPadding)
	return l
}

// Font returns a copy of this label current font
func (l *Label) Font() core.Font {

	return l.font
}

// SetText sets this label text
func (l *Label) SetText(text string) {

	l.text = text
	l.SetChanged(true)
}

// SetColor sets the color of the label text
func (l *Label) SetColor(color *core.Color) {

	l.color = *color
	l.SetChanged(true)
}

// SetFont sets the font of this label text
func (l *Label) SetFont(font *core.Font) {

	l.font = *font
	l.SetChanged(true)
}

func (l *Label) Render(c *dom.Canvas2d) {

	// Calculates the text width and height, sets this label panel content size
	// and render the base Panel.
	c.SetFont(l.font.String())
	tm := c.MeasureText(l.text)
	l.Panel.SetContentSize(tm.Width, l.font.Height())
	//fmt.Println("label render panel", tm.Width, l.font.Height())
	l.Panel.Render(c)

	// Draws the label text over the Panel content area
	//fmt.Println("label render text", tm.Width, l.font.Height())
	c.Save()
	l.Panel.ClipContent(c)
	c.SetTextBaseline(dom.TextBaselineTop)
	c.SetFillColor(l.color.String())
	x, y := l.AbsContentPos()
	c.FillText(l.text, x, y)
	c.Restore()
}

func (l *Label) recalc() {

}
