package widget

import (
	"fmt"
	"syscall/js"

	"gitlab.com/leonsal/gowebpanel/core"
	"gitlab.com/leonsal/gowebpanel/dom"
)

type Image struct {
	core.Panel           // embedded Panel
	img        dom.Image // dom image object
	loaded     bool      // image loaded flag
}

func NewImage() *Image {

	ip := new(Image)
	ip.Panel.Init(0, 0)
	ip.img.SetJsValue(dom.Document().CreateElement("img"))
	return ip
}

func (ip *Image) DomImage() *dom.Image {

	return &ip.img
}

func (ip *Image) Load(url string, cb func(err error)) {

	var jscb js.Func
	jscb = ip.img.AddEventListener(dom.OnLoad, func(event dom.Event, ev interface{}) {
		ip.img.RemoveEventListener(dom.OnLoad, jscb)
		jscb.Release()
		if event != dom.OnLoad {
			cb(fmt.Errorf("Error loading:%s", url))
			return
		}
		ip.loaded = true
		cb(nil)
	})
	ip.img.SetSrc(url)
}
