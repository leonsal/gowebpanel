package main

import (
	"fmt"

	"gitlab.com/leonsal/gowebpanel/dom"
	"gitlab.com/leonsal/gowebpanel/widget"
)

type TestImage struct {
}

func init() {
	testMap["image"] = &TestImage{}
}

func (t *TestImage) Run() {

	// Create canvas2d 1
	c1, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}
	c1.SetId("c1")

	// Create canvas2d 2
	c2, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}

	// Append canvases to document
	doc := dom.Document()
	container := doc.GetElementById("test")
	c1.AppendTo(container)
	hr := doc.CreateElement("hr")
	container.Call("appendChild", hr)
	c2.AppendTo(container)

	im := widget.NewImage()
	fmt.Printf("load image\n")
	im.Load("data/tiger1.jpg", func(err error) {
		if err != nil {
			fmt.Printf("Error loading image:%v\n", err)
			return
		}
		//img := im.DomImage().JsValue()
		//container.Call("appendChild", img)
		fmt.Println("Image loaded OK", im.Width(), im.Height())
		c1.DrawImage(im.DomImage(), 10, 10)
	})

}
