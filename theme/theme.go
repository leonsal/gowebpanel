package theme

import (
	"gitlab.com/leonsal/gowebpanel/core"
)

type Theme struct {
	Font            core.Font
	ColorNormalText core.Color
	LabelPadding    core.RectBounds
}

var _current *Theme

func init() {

	_current = themeLight()
}

func Current() *Theme {

	return _current
}

func themeLight() *Theme {

	var t Theme

	t.Font.
		SetStyle(core.FontStyleNormal).
		SetWeight(core.FontWeightNormal).
		SetSize(16, core.FontSizePx).
		SetFamily(core.FontFamilySansSerif)

	t.ColorNormalText.Set("black")
	t.LabelPadding = core.RectBounds{2, 0, 0, 1}
	return &t
}
