package dom

func FontHeight(size int, weight, families string) float64 {

	doc := Document()
	body := doc.GetElementsByTagName("body").Index(0)
	div := doc.CreateElement("div")

	div.Set("innerHTML", "Mj")
	style := div.Get("style")
	style.Set("position", "absolute")
	style.Set("top", "-100px")
	style.Set("left", "-100px")
	style.Set("fontFamily", families)
	style.Set("fontWeight", weight)
	style.Set("fontSize", size)

	body.Call("appendChild", div)
	result := div.Get("offsetHeight")
	body.Call("removeChild", div)

	return result.Float()
}
