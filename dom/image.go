package dom

type Image struct {
	HtmlElement
}

func (img *Image) SetSrc(src string) {

	img.jsv.Set("src", src)
}

func (img *Image) Height() int {

	return img.jsv.Get("height").Int()
}

func (img *Image) Width() int {

	return img.jsv.Get("width").Int()
}
