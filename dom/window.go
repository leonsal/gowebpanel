package dom

import (
	"syscall/js"
)

type WindowObj struct {
	EventTarget
	location *Location
}

type Location struct {
	jsv js.Value
}

// Window singleton instance
var _windowInstance *WindowObj

// Window returns the instance of the Window singleton object
func Window() *WindowObj {

	if _windowInstance != nil {
		return _windowInstance
	}

	w := new(WindowObj)
	w.jsv = js.Global().Get("window")
	_windowInstance = w
	return _windowInstance
}

// Location returns the window.location.href string
func (w *WindowObj) Location() *Location {

	if w.location != nil {
		return w.location
	}
	jsv := w.jsv.Get("location")
	w.location = &Location{jsv}
	return w.location
}

// InnerWidth returns the window content area width
func (w *WindowObj) InnerWidth() float64 {

	return w.jsv.Get("innerWidth").Float()
}

// InnerHeight returns the window content area height
func (w *WindowObj) InnerHeight() float64 {

	return w.jsv.Get("innerHeight").Float()
}

// OuterWidth returns the browser window width
func (w *WindowObj) OuterWidth() float64 {

	return w.jsv.Get("outerWidth").Float()
}

// OuterHeight returns the browser window height
func (w *WindowObj) OuterHeight() float64 {

	return w.jsv.Get("outerHeight").Float()
}

// RequestAnimationFrame requests the browser to call the specified callback
// before the next screen repaint. The callback is called with a timestamp
// in milliseconds.
func (w *WindowObj) RequestAnimationFrame(cb func(timestamp int)) {

	var jscb js.Func
	jscb = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		cb(args[0].Int())
		jscb.Release()
		return nil
	})
	w.jsv.Call("requestAnimationFrame", jscb)
}

//
// Location methods
//

func (l *Location) Href() string {

	return l.jsv.Get("href").String()
}

func (l *Location) Protocol() string {

	return l.jsv.Get("protocol").String()
}

func (l *Location) Host() string {

	return l.jsv.Get("host").String()
}

func (l *Location) Hostname() string {

	return l.jsv.Get("hostname").String()
}

func (l *Location) Port() string {

	return l.jsv.Get("port").String()
}

func (l *Location) Pathname() string {

	return l.jsv.Get("pathname").String()
}
