package dom

type HtmlElement struct {
	Element
}

func (he *HtmlElement) Hidden() bool {

	return he.jsv.Get("hidden").Bool()
}

func (he *HtmlElement) InnerText() string {

	return he.jsv.Get("innerText").String()
}

func (he *HtmlElement) SetInnerText(text string) {

	he.jsv.Set("innerText", text)
}
