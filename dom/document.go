package dom

import (
	"syscall/js"
)

type DocumentObj struct {
	jsdoc js.Value // Javascript document reference
}

// Document singleton instance
var _documentInstance *DocumentObj

// Document returns the instance of the Document singleton object
func Document() *DocumentObj {

	if _documentInstance != nil {
		return _documentInstance
	}

	d := new(DocumentObj)
	d.jsdoc = js.Global().Get("document")

	_documentInstance = d
	return _documentInstance
}

func (d *DocumentObj) CreateElement(el string) js.Value {

	return d.jsdoc.Call("createElement", el)
}

func (d *DocumentObj) GetElementById(id string) js.Value {

	return d.jsdoc.Call("getElementById", id)
}

func (d *DocumentObj) GetElementsByTagName(tag string) js.Value {

	return d.jsdoc.Call("getElementsByTagName", tag)
}

func (d *DocumentObj) CreateTextNode(text string) js.Value {

	return d.jsdoc.Call("createTextNode", text)
}
