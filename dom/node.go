package dom

import (
	"syscall/js"
)

type Node struct {
	EventTarget
}

type NodeList []*Node

func (n *Node) ChildNodes() NodeList {

	jsv := n.jsv.Get("childNodes")
	return newNodeList(jsv)
}

func (n *Node) AppendChild(node *Node) {

	n.jsv.Call("appendChild", node.jsv)
}

func newNodeList(v js.Value) NodeList {

	list := make(NodeList, v.Length())
	for i := range list {
		list[i] = &Node{EventTarget{v.Index(i)}}
	}
	return list
}
