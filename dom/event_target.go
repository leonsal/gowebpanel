package dom

import (
	"syscall/js"
)

type EventTarget struct {
	jsv js.Value
}

func (et *EventTarget) JsValue() js.Value {

	return et.jsv
}

func (et *EventTarget) AddEventListener(evtype Event, cb func(Event, interface{})) js.Func {

	jscb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		evtype, ev := buildEvent(args[0])
		cb(evtype, ev)
		return nil
	})
	et.jsv.Call("addEventListener", string(evtype), jscb)
	return jscb
}

//func (et *EventTarget) AddEventListenerOnce(evtype Event, cb func(Event, interface{})) js.Func {
//
//	jscb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
//		evtype, ev := buildEvent(args[0])
//		cb(evtype, ev)
//		return nil
//	})
//	et.jsv.Call("addEventListener", string(evtype), jscb)
//	return jscb
//}

func (et *EventTarget) RemoveEventListener(evtype Event, jscb js.Func) {

	et.jsv.Call("removeEventListener", string(evtype), jscb)
	jscb.Release()
}

func (et *EventTarget) AddEventListenerJs(evtype Event, jscb js.Func) {

	et.jsv.Call("addEventListener", string(evtype), jscb)
}
