package dom

import (
	"syscall/js"
)

type TextBaseline string

const (
	TextBaselineTop TextBaseline = "top"
)

type Canvas2d struct {
	HtmlElement          // embedded dom element
	ctx         js.Value // canvas context 2d
	width       float64  // current canvas width
	height      float64  // current canvas height
}

// New creates and returns a Canvas2d object with the specified width and height
func NewCanvas2d(width, height float64) (*Canvas2d, error) {

	canvas := Document().CreateElement("canvas")
	ctx := canvas.Call("getContext", "2d")

	c := new(Canvas2d)
	c.jsv = canvas
	c.ctx = ctx

	c.SetWidth(width)
	c.SetHeight(height)

	c.SetFillColor("gray")
	c.FillRect(0, 0, c.width, c.height)
	return c, nil
}

//func (c *Canvas2d) AddEventListener(evtype Event, cb func(Event, interface{})) js.Func {
//
//	jscb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
//		evtype, ev := buildEvent(args[0])
//		cb(evtype, ev)
//		return nil
//	})
//	c.canvas.Call("addEventListener", string(evtype), jscb)
//	return jscb
//}
//
//func (c *Canvas2d) RemoveEventListener(evtype Event, cb js.Func) {
//
//	c.canvas.Call("removeEventListener", string(evtype), cb)
//}

func (c *Canvas2d) SetTextBaseline(value TextBaseline) {

	c.ctx.Set("textBaseline", string(value))
}

func (c *Canvas2d) SetDispatchEvents(state bool) {

	//	// If desired state equals to current, nothing to do
	//	if c.dispatchEvents == state {
	//		return
	//	}
	//
	//	// List of events to listen/remove
	//	events := []Event{OnKeyDown, OnMouseDown, OnMouseUp, OnMouseMove, OnResize}
	//
	//	if state {
	//		// Add listeners to dispatch events
	//		for _, evtype := range events {
	//			c.listeners[evtype] = c.AddEventListener(evtype, func(evtype Event, ev interface{}) {
	//				c.Dispatch(evtype, ev)
	//			})
	//		}
	//	} else {
	//		// Remove event listeners and Release() js Funcs
	//		for evtype, jscb := range c.listeners {
	//			c.RemoveEventListener(evtype, jscb)
	//			jscb.Release()
	//		}
	//	}
	//	c.dispatchEvents = state
}

func (c *Canvas2d) BeginPath() {

	c.ctx.Call("beginPath")
}

func (c *Canvas2d) Restore() {

	c.ctx.Call("restore")
}

func (c *Canvas2d) Save() {

	c.ctx.Call("save")
}

func (c *Canvas2d) Height() float64 {

	return c.height
}

func (c *Canvas2d) Width() float64 {

	return c.width
}

func (c *Canvas2d) Clip() {

	c.ctx.Call("clip")
}

func (c *Canvas2d) DrawImage(im *Image, x, y float64) {

	c.ctx.Call("drawImage", im.JsValue(), x, y)
}

func (c *Canvas2d) Rect(x, y, width, height float64) {

	c.ctx.Call("rect", x, y, width, height)
}

func (c *Canvas2d) SetWidth(width float64) {

	c.jsv.Set("width", width)
	c.width = c.jsv.Get("width").Float()
}

func (c *Canvas2d) SetHeight(height float64) {

	c.jsv.Set("height", height)
	c.height = c.jsv.Get("height").Float()
}

func (c *Canvas2d) SetFont(font string) {

	c.ctx.Set("font", font)
}

//func (c *Canvas2d) SetFillStyle(style Color) {
//
//	c.ctx.Set("fillStyle", style.String())
//}

func (c *Canvas2d) SetFillColor(color string) {

	c.ctx.Set("fillStyle", color)
}

func (c *Canvas2d) FillRect(x, y, width, height float64) {

	c.ctx.Call("fillRect", x, y, width, height)
}

func (c *Canvas2d) FillText(text string, x, y float64) {

	c.ctx.Call("fillText", text, x, y)
}

// Appends this canvas as a child of the specified container
func (c *Canvas2d) AppendTo(container js.Value) {

	container.Call("appendChild", c.jsv)
}

type TextMetrics struct {
	Width float64
}

func (c *Canvas2d) MeasureText(text string) TextMetrics {

	jstm := c.ctx.Call("measureText", text)
	return TextMetrics{
		Width: jstm.Get("width").Float(),
	}
}
