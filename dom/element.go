package dom

import (
	"syscall/js"
)

type Element struct {
	Node
}

func (e *Element) SetJsValue(jsv js.Value) {

	e.jsv = jsv
}

func (e *Element) Id() string {

	return e.jsv.Get("id").String()
}

func (e *Element) SetId(id string) {

	e.jsv.Set("id", id)
}

func (e *Element) OffsetHeight() float64 {

	return e.jsv.Get("offsetHeight").Float()
}

func (e *Element) OffsetWidth() float64 {

	return e.jsv.Get("offsetWidth").Float()
}

func (e *Element) OffsetLeft() float64 {

	return e.jsv.Get("offsetLeft").Float()
}

func (e *Element) OffsetRight() float64 {

	return e.jsv.Get("offsetRight").Float()
}

func (e *Element) OffsetParent() float64 {

	return e.jsv.Get("offsetParent").Float()
}

func (e *Element) OffsetTop() float64 {

	return e.jsv.Get("offsetTop").Float()
}
