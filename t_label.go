package main

import (
	"fmt"
	"time"

	"gitlab.com/leonsal/gowebpanel/core"
	"gitlab.com/leonsal/gowebpanel/dom"
	"gitlab.com/leonsal/gowebpanel/icon"
	"gitlab.com/leonsal/gowebpanel/widget"
)

type TestLabel struct {
}

func init() {
	testMap["label"] = &TestLabel{}
}

func (t *TestLabel) Run() {

	// Create canvas2d 1
	c1, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}
	c1.SetId("c1")

	// Create canvas2d 2
	c2, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}

	// Append canvases to document
	doc := dom.Document()
	container := doc.GetElementById("test")
	c1.AppendTo(container)
	hr := doc.CreateElement("hr")
	container.Call("appendChild", hr)
	c2.AppendTo(container)

	// Creates root panels
	root1 := t.buildRoot()
	//root2 := t.buildRoot()

	// Manage canvas events to panels
	pm := core.Manager()
	pm.Manage(c1, root1)
	//pm.Manage(c2, root2)

	// Render loop
	pm.StartRender()
}

func (t *TestLabel) buildRoot() *core.Panel {

	root := core.NewPanel(0, 0)
	root.SetBgColor(core.NewColor("blue"))
	root.SetPos(50, 50)
	root.SetMargin(10, 10, 10, 10)
	root.SetBorderColor(core.NewColor("red"))
	root.SetBorder(10, 10, 10, 10)
	root.SetBgColor(core.NewColor("green"))
	root.SetPaddingColor(core.NewColor("white"))
	root.SetPadding(10, 10, 10, 10)
	root.SetSize(700, 350)

	label1 := widget.NewLabel("AbcdefghijklmnopqrstuvwzyZ")
	label1.SetBorder(2, 2, 2, 2)
	label1.SetBorderColor(core.NewColor("red"))
	label1.SetPos(10, 10)
	label1.SetBgColor(core.NewColor("white"))
	root.Add(label1)

	label2 := widget.NewLabel("Very long label clipped by the parent panel")
	label2.SetBorder(1, 1, 1, 1)
	label2.SetBorderColor(core.NewColor("black"))
	label2.SetPos(402, 50)
	//label2.SetBgColor(core.NewColor("white"))
	root.Add(label2)

	font := core.NewFont(16, core.FontSizePt, "Material icons")
	label3 := widget.NewLabel(icon.AddCircle + icon.Check + icon.CheckBox)
	label3.SetPos(10, 200)
	label3.SetBorder(1, 1, 1, 1)
	label3.SetBorderColor(core.NewColor("black"))
	label3.SetFont(font)
	root.Add(label3)

	label4 := widget.NewLabel("")
	label4.SetPos(10, 100)
	//label4.SetBorder(1, 1, 1, 1)
	//label4.SetBorderColor(core.NewColor("white"))
	label4.SetBgColor(core.NewColor("white"))
	root.Add(label4)

	counter := 0
	go func() {
		for {
			text := fmt.Sprintf("%04d", counter)
			label4.SetText(text)
			fmt.Printf("counter:%s\n", text)
			counter++
			time.Sleep(100 * time.Millisecond)
		}
	}()
	return root
}
