package main

import (
	"gitlab.com/leonsal/gowebpanel/core"
	"gitlab.com/leonsal/gowebpanel/dom"
)

type TestPanel struct {
}

func init() {
	testMap["panel"] = &TestPanel{}
}

func (t *TestPanel) Run() {

	// Create canvas2d 1
	c1, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}
	c1.SetDispatchEvents(true)

	// Create canvas2d 2
	c2, err := dom.NewCanvas2d(1000, 400)
	if err != nil {
		panic(err)
	}
	c2.SetDispatchEvents(true)

	// Append canvases to document
	doc := dom.Document()
	container := doc.GetElementById("test")
	c1.AppendTo(container)
	hr := doc.CreateElement("hr")
	container.Call("appendChild", hr)
	c2.AppendTo(container)

	// Creates root panels
	root1 := t.buildRoot()
	//root2 := t.buildRoot()

	// Manage canvas events to panels
	pm := core.Manager()
	pm.Manage(c1, root1)
	//pm.Manage(c2, root2)

	// Render loop
	pm.StartRender()
}

func (t *TestPanel) buildRoot() *core.Panel {

	newChild := func(bounded bool) *core.Panel {
		p := core.NewPanel(100, 100)
		p.SetMargin(2, 2, 2, 2)
		p.SetMarginColor(core.NewColor("yellow"))
		p.SetBorder(2, 2, 2, 2)
		p.SetBorderColor(core.NewColor("black"))
		p.SetBgColor(core.NewColor("blue"))
		p.SetPadding(2, 2, 2, 2)
		p.SetPaddingColor(core.NewColor("green"))
		p.SetPos(-20, -20)

		pTL := core.NewPanel(100, 100)
		pTL.SetMargin(2, 2, 2, 2)
		pTL.SetMarginColor(core.NewColor("black"))
		pTL.SetBorder(2, 2, 2, 2)
		pTL.SetBorderColor(core.NewColor("red"))
		pTL.SetBgColor(core.NewColor("orange"))
		pTL.SetPadding(2, 2, 2, 2)
		pTL.SetPaddingColor(core.NewColor("blue"))
		pTL.SetPos(-70, -70)
		pTL.SetBounded(bounded)
		p.Add(pTL)

		pTR := core.NewPanel(100, 100)
		pTR.SetMargin(2, 2, 2, 2)
		pTR.SetMarginColor(core.NewColor("black"))
		pTR.SetBorder(2, 2, 2, 2)
		pTR.SetBorderColor(core.NewColor("red"))
		pTR.SetBgColor(core.NewColor("orange"))
		pTR.SetPadding(2, 2, 2, 2)
		pTR.SetPaddingColor(core.NewColor("blue"))
		pTR.SetPos(60, -70)
		pTR.SetBounded(bounded)
		p.Add(pTR)

		pBL := core.NewPanel(100, 100)
		pBL.SetMargin(2, 2, 2, 2)
		pBL.SetMarginColor(core.NewColor("black"))
		pBL.SetBorder(2, 2, 2, 2)
		pBL.SetBorderColor(core.NewColor("red"))
		pBL.SetBgColor(core.NewColor("orange"))
		pBL.SetPadding(2, 2, 2, 2)
		pBL.SetPaddingColor(core.NewColor("blue"))
		pBL.SetPos(-70, 60)
		pBL.SetBounded(bounded)
		p.Add(pBL)

		pBR := core.NewPanel(100, 100)
		pBR.SetMargin(2, 2, 2, 2)
		pBR.SetMarginColor(core.NewColor("black"))
		pBR.SetBorder(2, 2, 2, 2)
		pBR.SetBorderColor(core.NewColor("red"))
		pBR.SetBgColor(core.NewColor("orange"))
		pBR.SetPadding(2, 2, 2, 2)
		pBR.SetPaddingColor(core.NewColor("blue"))
		pBR.SetPos(60, 60)
		pBR.SetBounded(bounded)
		p.Add(pBR)

		return p
	}

	root := core.NewPanel(0, 0)
	root.SetMargin(2, 2, 2, 2)
	root.SetPos(40, 40)
	root.SetMargin(10, 10, 10, 10)
	root.SetMarginColor(core.NewColor("black"))
	root.SetBorder(10, 10, 10, 10)
	root.SetBorderColor(core.NewColor("red"))
	root.SetPadding(10, 10, 10, 10)
	//root.SetBgColor(core.NewColor("green"))
	root.SetPaddingColor(core.NewColor("white"))
	root.SetSize(700, 350)

	cTL := newChild(true)
	cTL.SetPos(-20, -20)
	root.Add(cTL)

	cTR := newChild(true)
	cTR.SetPos(550, -20)
	root.Add(cTR)

	cBL := newChild(true)
	cBL.SetPos(-20, 200)
	root.Add(cBL)

	cBR := newChild(true)
	cBR.SetPos(550, 200)
	root.Add(cBR)

	cML := newChild(true)
	cML.SetPos(150, 100)
	root.Add(cML)

	cMR := newChild(false)
	cMR.SetPos(370, 100)
	root.Add(cMR)

	return root
}
