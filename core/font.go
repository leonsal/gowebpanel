package core

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/leonsal/gowebpanel/dom"
)

type FontStyle string

const (
	FontStyleNormal  FontStyle = "normal"
	FontStyleItalic  FontStyle = "italic"
	FontStyleOblique FontStyle = "oblique"
)

type FontWeight string

const (
	FontWeightNormal  FontWeight = "normal"
	FontWeightBold    FontWeight = "bold"
	FontWeightBolder  FontWeight = "bolder"
	FontWeightLighter FontWeight = "lighter"
	FontWeight100     FontWeight = "100"
	FontWeight200     FontWeight = "200"
	FontWeight300     FontWeight = "300"
	FontWeight400     FontWeight = "400"
	FontWeight500     FontWeight = "500"
	FontWeight600     FontWeight = "600"
	FontWeight700     FontWeight = "700"
	FontWeight800     FontWeight = "800"
	FontWeight900     FontWeight = "900"
)

type FontSizeUnit string

const (
	FontSizePx FontSizeUnit = "px"
	FontSizePt FontSizeUnit = "pt"
)

type FontFamily string

const (
	FontFamilySerif     FontFamily = "serif"
	FontFamilySansSerif FontFamily = "sans-serif"
	FontFamilyCursive   FontFamily = "cursive"
	FontFamilyFantasy   FontFamily = "fantasy"
	FontFamilyMonospace FontFamily = "monospace"
)

type Font struct {
	style    FontStyle    // normal, italic, oblique
	weight   FontWeight   // normal, bold, bolder, lighter, 100,...,900
	size     uint         // size in pixels or points
	sizeUnit FontSizeUnit // px or pt
	families []string     // serif, sans-serif, cursive, fantasy, monospace, others
	str      string       // cached formatted font string
	height   float64      // cached calculated height
}

// NewFont creates and returns a font specification with the specified size, unit and family
func NewFont(size uint, unit FontSizeUnit, family FontFamily) *Font {

	f := new(Font)
	f.SetStyle(FontStyleNormal).
		SetWeight(FontWeightNormal).
		SetSize(size, unit).
		SetFamily(family)
	return f
}

func (f *Font) Style() FontStyle {

	return f.style
}

func (f *Font) SetStyle(style FontStyle) *Font {

	f.style = style
	f.str = ""
	f.height = 0
	return f
}

func (f *Font) SetWeight(weight FontWeight) *Font {

	f.weight = weight
	f.str = ""
	f.height = 0
	return f
}

func (f *Font) SetSize(size uint, unit FontSizeUnit) *Font {

	f.size = size
	f.sizeUnit = unit
	f.str = ""
	f.height = 0
	return f
}

func (f *Font) SetFamily(family FontFamily) *Font {

	f.families = []string{string(family)}
	f.str = ""
	f.height = 0
	return f
}

func (f *Font) AddFamily(family FontFamily) *Font {

	f.families = append(f.families, string(family))
	f.str = ""
	f.height = 0
	return f
}

// String returns this font CSS string specification:
// {style} {weight} {size}px|pt {family}[,{family},...,{family}]
func (f *Font) String() string {

	if f.str == "" {
		families := strings.Join(f.families, ",")
		f.str = fmt.Sprintf("%s %s %d%s %s", f.style, f.weight, f.size, f.sizeUnit, families)
	}
	return f.str
}

// Height returns this font height in pixels
func (f *Font) Height() float64 {

	if f.height != 0 {
		return f.height
	}

	families := strings.Join(f.families, ",")
	size := strconv.Itoa(int(f.size)) + string(f.sizeUnit)

	doc := dom.Document()
	body := doc.GetElementsByTagName("body").Index(0)
	div := doc.CreateElement("div")

	div.Set("innerHTML", "Mj")
	style := div.Get("style")
	style.Set("position", "absolute")
	style.Set("top", "-100px")
	style.Set("left", "-100px")
	style.Set("fontFamily", families)
	style.Set("fontWeight", string(f.weight))
	style.Set("fontSize", size)

	body.Call("appendChild", div)
	result := div.Get("offsetHeight")
	body.Call("removeChild", div)

	f.height = result.Float()
	return f.height
}
