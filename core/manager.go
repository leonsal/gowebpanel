package core

import (
	"fmt"
	"syscall/js"

	"gitlab.com/leonsal/gowebpanel/dom"
)

type pmState struct {
	panel     *Panel                // panel associated with the managed canvas
	listeners map[dom.Event]js.Func // installed canvas listeners
}

type ManagerObj struct {
	managed       map[*dom.Canvas2d]*pmState
	renderStarted bool
}

// Manager singleton instance
var _managerInstance *ManagerObj

// Manager returns the instance of the panel manager singleton object
func Manager() *ManagerObj {

	if _managerInstance != nil {
		return _managerInstance
	}

	pm := new(ManagerObj)
	pm.managed = make(map[*dom.Canvas2d]*pmState)

	// add event listeners to the dom Window.
	dom.Window().AddEventListener(dom.OnKeyDown, func(evtype dom.Event, ev interface{}) {
		pm.onKey(ev.(dom.EventKey))
	})
	dom.Window().AddEventListener(dom.OnKeyPress, func(evtype dom.Event, ev interface{}) {
		pm.onKey(ev.(dom.EventKey))
	})
	dom.Window().AddEventListener(dom.OnKeyUp, func(evtype dom.Event, ev interface{}) {
		pm.onKey(ev.(dom.EventKey))
	})

	_managerInstance = pm
	return _managerInstance
}

// Manage process events from the specified canvas and sends to the specified root panel
// Pass a nil panel to stop managing the specified canvas events.
func (pm *ManagerObj) Manage(c *dom.Canvas2d, p *Panel) {

	state, ok := pm.managed[c]
	if ok {
		if p != nil {
			// Update managed panel
			state.panel = p
		} else {
			// Remove canvas event listeners
			for event, jscb := range state.listeners {
				c.RemoveEventListener(event, jscb)
			}
			delete(pm.managed, c)
		}
		return
	}

	// Add canvas to be managed
	pm.managed[c] = &pmState{p, map[dom.Event]js.Func{}}

	// Add event listeners
	var jscb js.Func
	jscb = c.AddEventListener(dom.OnMouseDown, func(evtype dom.Event, ev interface{}) {
		pm.onMouseDown(c, ev.(dom.EventMouse))
	})
	pm.managed[c].listeners[dom.OnMouseDown] = jscb

	jscb = c.AddEventListener(dom.OnMouseUp, func(evtype dom.Event, ev interface{}) {
		pm.onMouseUp(c, ev.(dom.EventMouse))
	})
	pm.managed[c].listeners[dom.OnMouseUp] = jscb
}

// Clear removes all managed canvas and panels
func (pm *ManagerObj) Clear() {

	for c, state := range pm.managed {
		// Remove canvas event listeners
		for event, jscb := range state.listeners {
			c.RemoveEventListener(event, jscb)
		}
		delete(pm.managed, c)
	}
}

// StartRender starts rendering all the managed canvas with the respective panels
func (pm *ManagerObj) StartRender() {

	window := dom.Window().JsValue()
	var render js.Func
	pm.renderStarted = true

	render = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		if !pm.renderStarted {
			render.Release()
			return nil
		}
		//timestamp := args[0].Int()
		//fmt.Printf("render:%v\n", timestamp)
		for c, state := range pm.managed {
			//if state.panel.Changed() {
			state.panel.Render(c)
			//}
		}
		window.Call("requestAnimationFrame", render)
		return nil
	})
	window.Call("requestAnimationFrame", render)
}

func (pm *ManagerObj) StopRender() {

	pm.renderStarted = false
}

func (pm *ManagerObj) onKey(ev dom.EventKey) {

	fmt.Printf("onKey:%v\n", ev)
}

// onMouseDown process mousedown events for the specified canvas
func (pm *ManagerObj) onMouseDown(c *dom.Canvas2d, ev dom.EventMouse) {

	p := pm.managed[c]
	fmt.Printf("onMouseDown canvas:%p panel:%p\n", c, p)
}

// onMouseUp process mouseup events for the specified canvas
func (pm *ManagerObj) onMouseUp(c *dom.Canvas2d, ev dom.EventMouse) {

	p := pm.managed[c]
	fmt.Printf("onMouseUp canvas:%p panel:%p\n", c, p)
}
