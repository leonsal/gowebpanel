package core

// RectBounds specifies the size of the boundaries of a rectangle.
// It can represent the thickness of the borders, the margins, or the padding of a rectangle.
type RectBounds struct {
	Top    float64
	Right  float64
	Bottom float64
	Left   float64
}

// Set sets the values of the RectBounds.
func (bs *RectBounds) Set(top, right, bottom, left float64) {

	if top >= 0 {
		bs.Top = top
	}
	if right >= 0 {
		bs.Right = right
	}
	if bottom >= 0 {
		bs.Bottom = bottom
	}
	if left >= 0 {
		bs.Left = left
	}
}

// IsZero return if all the this rectangle's sides has zero width.
func (rb *RectBounds) IsZero() bool {

	if rb.Top == 0 && rb.Right == 0 && rb.Bottom == 0 && rb.Left == 0 {
		return true
	}
	return false
}

// Rect represents a rectangle.
type Rect struct {
	X      float64
	Y      float64
	Width  float64
	Height float64
}

// Contains determines whether a 2D point is inside the Rect.
func (r *Rect) Contains(x, y float64) bool {

	if x < r.X || x > r.X+r.Width {
		return false
	}
	if y < r.Y || y > r.Y+r.Height {
		return false
	}
	return true
}
