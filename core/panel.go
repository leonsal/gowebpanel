package core

import (
	"math"

	"gitlab.com/leonsal/gowebpanel/dom"
)

/****************************************************

     Panel areas:
     +------------------------------------------+
     |  Margin area                             |
     |  +------------------------------------+  |
     |  |  Border area                       |  |
     |  |  +------------------------------+  |  |
     |  |  | Padding area                 |  |  |
     |  |  |  +------------------------+  |  |  |
     |  |  |  | Content area           |  |  |  |
     |  |  |  |                        |  |  |  |
     |  |  |  |                        |  |  |  |
     |  |  |  +------------------------+  |  |  |
     |  |  |                              |  |  |
     |  |  +------------------------------+  |  |
     |  |                                    |  |
     |  +------------------------------------+  |
     |                                          |
     +------------------------------------------+

****************************************************/

// IPanel is the interface for all panel types
type IPanel interface {
	GetPanel() *Panel
	Render(c *dom.Canvas2d)
	//LostKeyFocus()
	//TotalHeight() float32
	//TotalWidth() float32
	//SetLayout(ILayout)
	//SetPosition(x, y float32)
	//SetPositionX(x float32)
	//SetPositionY(y float32)
}

type Panel struct {
	Dispatcher               // embedded dispatcher
	width        float64     // external panel width
	height       float64     // external panel height
	x            float64     // panel top left x coordinate relative to parent
	y            float64     // panel top left y coordinate relative to parent
	z            float64     // panel z order
	parent       IPanel      // parent panel
	children     []IPanel    // list of children panels
	marginSizes  RectBounds  // external margin sizes in pixel coordinates
	borderSizes  RectBounds  // border sizes in pixel coordinates
	paddingSizes RectBounds  // padding sizes in pixel coordinates
	content      Rect        // current content rectangle in pixel coordinates
	marginColor  Color       // current margin color
	borderColor  Color       // current border color
	paddingColor Color       // current padding color
	contentColor Color       // current content color
	visible      bool        // visibility state
	bounded      bool        // indicates if panel is bounded by its parent or not
	enabled      bool        // enable/disable event processing
	changed      bool        // indicates if panel was changed after last render
	xabs         float64     // absolute x coordinate
	yabs         float64     // absolute y coordinate
	xabsMin      float64     // absolute minimum x coordinate
	xabsMax      float64     // absolute maximum x coordinate
	yabsMin      float64     // absolute minimum y coordinate
	yabsMax      float64     // absolute maximum y coordinate
	userData     interface{} // generic user data
}

// NewPanel creates and returns a Panel object with the specified width and height
func NewPanel(width, height float64) *Panel {

	p := new(Panel)
	p.Init(width, height)
	return p
}

// AbsContentPos returns the absolute position of this panel content in the last canvas it was rendered.
func (p *Panel) AbsContentPos() (float64, float64) {

	return p.xabs + p.marginSizes.Left + p.borderSizes.Left + p.paddingSizes.Left,
		p.yabs + p.marginSizes.Top + p.borderSizes.Top + p.paddingSizes.Top
}

// AbsPos returns the absolute position of this panel in the last canvas it was rendered.
func (p *Panel) AbsPos() (float64, float64) {

	return p.xabs, p.yabs
}

// Add adds the specified panel to the list of this panel's children and sets its parent pointer.
// If the specified panel had a parent, the specified panel is removed from the original parent's list of children.
func (p *Panel) Add(ichild IPanel) *Panel {

	p.setParentOf(ichild)
	p.children = append(p.children, ichild)
	p.changed = true
	return p
}

// Borders returns this panel current border sizes
func (p *Panel) Borders() RectBounds {

	return p.borderSizes
}

// Bounded returns this panel bounded state
func (p *Panel) Bounded() bool {

	return p.bounded
}

// Children returns the list of this panel's children.
func (p *Panel) Children() []IPanel {

	return p.children
}

// Changed returns if this panel was changes after its last render
func (p *Panel) Changed() bool {

	return p.changed
}

// Clip sets a clip region in the specified canvas to this panel external area
func (p *Panel) ClipContent(c *dom.Canvas2d) {

	x := math.Max(p.xabsMin, p.xabs+p.calcLeftBorders())
	y := math.Max(p.yabsMin, p.yabs+p.calcTopBorders())
	width := math.Min(p.content.Width, p.xabsMax-p.calcRightBorders()-p.xabsMin)
	height := math.Min(p.content.Height, p.yabsMax-p.calcBottomBorders()-p.yabsMin)
	c.BeginPath()
	c.Rect(x, y, width, height)
	c.Clip()
	//fmt.Printf("ClipContent:%v %v %v %v - %v %v %v %v\n", x, y, width, height, p.xabsMin, p.xabsMax, p.yabsMin, p.yabsMax)
}

// ContainsPosition returns indication if this panel contains
// the specified absolute canvas position in pixels.
func (p *Panel) ContainsPosition(x, y float64) bool {

	if x < p.xabs || x >= (p.xabs+p.width) {
		return false
	}
	if y < p.yabs || y >= (p.yabs+p.height) {
		return false
	}
	return true
}

// ContentHeight returns the current height of the content area in pixels
func (p *Panel) ContentHeight() float64 {

	return p.content.Height
}

// ContentWidth returns the current width of the content area in pixels
func (p *Panel) ContentWidth() float64 {

	return p.content.Width
}

// GetPanel satisfies the IPanel interface and returns pointer to this panel
func (p *Panel) GetPanel() *Panel {

	return p
}

// Enabled returns the current enabled state of this panel
func (p *Panel) Enabled() bool {

	return p.enabled
}

// Height returns the current panel external height in pixels
func (p *Panel) Height() float64 {

	return p.height
}

func (p *Panel) ListenEvents(c *dom.Canvas2d) {

}

// Margins returns the current margin sizes in pixels
func (p *Panel) Margins() RectBounds {

	return p.marginSizes
}

// Paddings returns this panel padding sizes in pixels
func (p *Panel) Paddings() RectBounds {

	return p.paddingSizes
}

// Parent returns this panel's parent panel
func (p *Panel) Parent() IPanel {

	return p.parent
}

// Remove removes the specified IPanel from the list of this panel's children.
// Returns true if found or false otherwise.
func (p *Panel) Remove(ichild IPanel) bool {

	for pos, current := range p.children {
		if current == ichild {
			copy(p.children[pos:], p.children[pos+1:])
			p.children[len(p.children)-1] = nil
			p.children = p.children[:len(p.children)-1]
			ichild.GetPanel().parent = nil
			//p.Dispatch(OnChild, nil)
			p.changed = true
			return true
		}
	}
	return false
}

// Render draws this panel and all its children on the specified canvas
// It is normally called by the the root's panel Render() method.
func (p *Panel) Render(c *dom.Canvas2d) {

	// If panel is not visible do not render it and none of its children
	if !p.visible {
		return
	}

	// Updates this panel absolute coordinates relative to its parent
	p.updateBounds()

	// Initializes this panel external absolute coordinates and maximum width and height
	x := p.xabs
	y := p.yabs

	if p.parent != nil && p.bounded {
		c.Save()
		p.parent.GetPanel().ClipContent(c)
	}

	// Draw margin rectangles
	if p.marginColor.A() != 0 {
		c.SetFillColor(p.marginColor.String())
		if p.marginSizes.Top != 0 {
			c.FillRect(x, y, p.width, p.marginSizes.Top)
		}
		if p.marginSizes.Right != 0 {
			c.FillRect(x+p.width-p.marginSizes.Right, y, p.marginSizes.Right, p.height)
		}
		if p.marginSizes.Bottom != 0 {
			c.FillRect(x, y+p.height-p.marginSizes.Bottom, p.width, p.marginSizes.Bottom)
		}
		if p.marginSizes.Left != 0 {
			c.FillRect(x, y, p.marginSizes.Left, p.height)
		}
	}
	x += p.marginSizes.Left
	y += p.marginSizes.Top

	// Draw border rectangles
	if p.borderColor.A() != 0 {
		c.SetFillColor(p.borderColor.String())
		width := p.calcBorderWidth()
		height := p.calcBorderHeight()
		if p.borderSizes.Top != 0 {
			c.FillRect(x, y, width, p.borderSizes.Top)
		}
		if p.borderSizes.Right != 0 {
			c.FillRect(x+width-p.borderSizes.Right, y, p.borderSizes.Right, height)
		}
		if p.borderSizes.Bottom != 0 {
			c.FillRect(x, y+height-p.borderSizes.Bottom, width, p.borderSizes.Bottom)
		}
		if p.borderSizes.Left != 0 {
			c.FillRect(x, y, p.borderSizes.Left, height)
		}
	}
	x += p.borderSizes.Left
	y += p.borderSizes.Top

	// Draw padding rectangles
	if p.paddingColor.A() != 0 {
		c.SetFillColor(p.paddingColor.String())
		width := p.calcPaddingWidth()
		height := p.calcPaddingHeight()
		if p.paddingSizes.Top != 0 {
			c.FillRect(x, y, width, p.paddingSizes.Top)
		}
		if p.paddingSizes.Right != 0 {
			c.FillRect(x+width-p.paddingSizes.Right, y, p.paddingSizes.Right, height)
		}
		if p.paddingSizes.Bottom != 0 {
			c.FillRect(x, y+height-p.paddingSizes.Bottom, width, p.paddingSizes.Bottom)
		}
		if p.paddingSizes.Left != 0 {
			c.FillRect(x, y, p.paddingSizes.Left, height)
		}
	}
	x += p.paddingSizes.Left
	y += p.paddingSizes.Top

	// Draw content
	if p.contentColor.A() != 0 {
		c.SetFillColor(p.contentColor.String())
		c.FillRect(x, y, p.content.Width, p.content.Height)
	}

	if p.parent != nil && p.bounded {
		c.Restore()
	}

	// Render all this panel's children
	for i := 0; i < len(p.children); i++ {
		child := p.children[i]
		child.Render(c)
	}
	p.changed = false
}

// SetBorder sets this panel border sizes in pixels
// and recalculates the panel external size
func (p *Panel) SetBorder(top, right, bottom, left float64) {

	p.borderSizes.Set(top, right, bottom, left)
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetBorderFrom sets this panel border sizes from the specified
// RectBounds pointer and recalculates the panel size
func (p *Panel) SetBorderFrom(src *RectBounds) {

	p.borderSizes = *src
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetBorderColor sets the color of this panel border using the RGB model.
// The border opacity is set to 1.0 (full opaque)
func (p *Panel) SetBorderColor(color *Color) {

	p.borderColor = *color
	p.changed = true
}

// SetBounded sets this panel bounded state
func (p *Panel) SetBounded(bounded bool) {

	p.bounded = bounded
	p.changed = true
}

// SetChanged sets this panel changed flag
func (p *Panel) SetChanged(changed bool) {

	p.changed = changed
}

// SetContentAspectHeight sets the height of the content area of the panel
// to the specified value and adjusts its width to keep the same aspect ratio.
func (p *Panel) SetContentAspectHeight(height float64) {

	aspect := p.content.Width / p.content.Height
	width := height / aspect
	p.SetContentSize(width, height)
}

// SetContentAspectWidth sets the width of the content area of the panel
// to the specified value and adjusts its height to keep the same aspect radio.
func (p *Panel) SetContentAspectWidth(width float64) {

	aspect := p.content.Width / p.content.Height
	height := width / aspect
	p.SetContentSize(width, height)
}

// SetContentHeight sets this panel content height to the specified dimension in pixels.
// The external size of the panel may increase or decrease to accommodate the new width
func (p *Panel) SetContentHeight(height float64) {

	p.SetContentSize(p.content.Width, height)
}

// SetContentSize sets this panel content size to the specified dimensions.
// The external size of the panel may increase or decrease to acomodate
// the new content size.
func (p *Panel) SetContentSize(width, height float64) {

	p.setContentSize(width, height, true)
}

// SetContentWidth sets this panel content width to the specified dimension in pixels.
// The external size of the panel may increase or decrease to accommodate the new width
func (p *Panel) SetContentWidth(width float64) {

	p.SetContentSize(width, p.content.Height)
}

// SetEnabled sets the panel enabled state
// A disabled panel do not process key or mouse events.
func (p *Panel) SetEnabled(state bool) {

	p.enabled = state
	p.changed = true
	//p.Dispatch(OnEnable, nil)
}

// SetHeight sets this panel external height in pixels.
// The internal panel areas and positions are recalculated
func (p *Panel) SetHeight(height float64) {

	p.SetSize(p.width, height)
}

// SetMargin sets this panel margin sizes in pixels
// and recalculates the panel external size
func (p *Panel) SetMargin(top, right, bottom, left float64) {

	p.marginSizes.Set(top, right, bottom, left)
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetMarginFrom sets this panel margins sizes from the specified
// RectBounds pointer and recalculates the panel external size
func (p *Panel) SetMarginFrom(src *RectBounds) {

	p.marginSizes = *src
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetMarginColor sets the color of this panel margins.
// By default the margins are transparent.
func (p *Panel) SetMarginColor(color *Color) {

	p.marginColor = *color
	p.changed = true
}

// SetPadding sets the panel padding sizes in pixels
func (p *Panel) SetPadding(top, right, bottom, left float64) {

	p.paddingSizes.Set(top, right, bottom, left)
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetPaddingFrom sets this panel padding sizes from the specified
// RectBounds pointer and recalculates the panel size
func (p *Panel) SetPaddingFrom(src *RectBounds) {

	p.paddingSizes = *src
	p.resize(p.calcWidth(), p.calcHeight(), true)
}

// SetPaddingColor sets the color of this panel paddings.
func (p *Panel) SetPaddingColor(color *Color) {

	p.paddingColor = *color
	p.changed = true
}

// SetPos sets this panel absolute position in pixel coordinates
// from left to right and from top to bottom of the canvas.
func (p *Panel) SetPos(x, y float64) {

	p.x = math.Round(x)
	p.y = math.Round(y)
	p.changed = true
}

// SetRGB sets the color of the panel paddings and content area
// The padding/content opacity is set to 1.0 (full opaque)
func (p *Panel) SetBgColor(color *Color) *Panel {

	p.paddingColor = *color
	p.contentColor = *color
	p.changed = true
	return p
}

// SetSize sets this panel external width and height in pixels.
func (p *Panel) SetSize(width, height float64) {

	if width < 0 {
		width = 0
	}
	if height < 0 {
		height = 0
	}
	p.resize(width, height, true)
}

// SetVisible sets this panel visibility state.
func (p *Panel) SetVisible(visible bool) {

	p.visible = visible
	p.changed = true
}

// SetWidth sets this panel external width in pixels.
// The internal panel areas and positions are recalculated
func (p *Panel) SetWidth(width float64) {

	p.SetSize(width, p.height)
}

// Size returns this panel current external width and height in pixels
func (p *Panel) Size() (float64, float64) {

	return p.width, p.height
}

// Visible returns this panel's visibility state
func (p *Panel) Visible() bool {

	return p.visible
}

// Width returns the current panel external width in pixels
func (p *Panel) Width() float64 {

	return p.width
}

// calcBorderWidth calculates this panel border rectangle width in pixels
func (p *Panel) calcBorderWidth() float64 {

	return p.content.Width +
		p.paddingSizes.Left + p.paddingSizes.Right +
		p.borderSizes.Left + p.borderSizes.Right
}

// calcBorderHeight calculates this panel border rectangle height in pixels
func (p *Panel) calcBorderHeight() float64 {

	return p.content.Height +
		p.paddingSizes.Top + p.paddingSizes.Bottom +
		p.borderSizes.Top + p.borderSizes.Bottom
}

func (p *Panel) calcLeftBorders() float64 {

	return p.marginSizes.Left + p.borderSizes.Left + p.paddingSizes.Left
}

func (p *Panel) calcRightBorders() float64 {

	return p.marginSizes.Right + p.borderSizes.Right + p.paddingSizes.Right
}

func (p *Panel) calcTopBorders() float64 {

	return p.marginSizes.Top + p.borderSizes.Top + p.paddingSizes.Top
}

func (p *Panel) calcBottomBorders() float64 {

	return p.marginSizes.Bottom + p.borderSizes.Bottom + p.paddingSizes.Bottom
}

// calcPaddingWidth calculates this panel padding rectangle width in pixels
func (p *Panel) calcPaddingWidth() float64 {

	return p.content.Width +
		p.paddingSizes.Left + p.paddingSizes.Right
}

// calcPaddingHeight calculates this panel padding rectangle height in pixels
func (p *Panel) calcPaddingHeight() float64 {

	return p.content.Height +
		p.paddingSizes.Top + p.paddingSizes.Bottom
}

// calcWidth calculates the panel external width in pixels
func (p *Panel) calcWidth() float64 {

	return p.content.Width +
		p.paddingSizes.Left + p.paddingSizes.Right +
		p.borderSizes.Left + p.borderSizes.Right +
		p.marginSizes.Left + p.marginSizes.Right
}

// calcHeight calculates the panel external height in pixels
func (p *Panel) calcHeight() float64 {

	return p.content.Height +
		p.paddingSizes.Top + p.paddingSizes.Bottom +
		p.borderSizes.Top + p.borderSizes.Bottom +
		p.marginSizes.Top + p.marginSizes.Bottom
}

// Init initializes this panel.
// It is normally used by other widgets which embed a Panel
func (p *Panel) Init(width, height float64) {

	p.width = width
	p.height = height

	p.visible = true
	p.bounded = true
	p.enabled = true
	p.changed = true
	p.resize(width, height, true)
}

// resize tries to set the external size of the panel to the specified
// dimensions and recalculates the size and positions of the internal areas.
// The margins, borders and padding sizes are kept and the content
// area size is adjusted. So if the panel is decreased, its minimum
// size is determined by the margins, borders and paddings.
// Normally it should be called with dispatch=true to recalculate the
// panel layout and dispatch OnSize event.
func (p *Panel) resize(width, height float64, dispatch bool) {

	// Adjusts content width
	p.content.Width = width -
		p.marginSizes.Left - p.marginSizes.Right -
		p.borderSizes.Left - p.borderSizes.Right -
		p.paddingSizes.Left - p.paddingSizes.Right
	if p.content.Width < 0 {
		p.content.Width = 0
	}

	// Adjusts content height
	p.content.Height = height -
		p.marginSizes.Top - p.marginSizes.Bottom -
		p.borderSizes.Top - p.borderSizes.Bottom -
		p.paddingSizes.Top - p.paddingSizes.Bottom
	if p.content.Height < 0 {
		p.content.Height = 0
	}

	// Sets final external size
	p.width = p.calcWidth()
	p.height = p.calcHeight()
	p.changed = true

	//// Update layout and dispatch event
	//if !dispatch {
	//	return
	//}
	//if p.layout != nil {
	//	p.layout.Recalc(p)
	//}
	//p.Dispatch(OnResize, nil)
}

// setContentSize is an internal version of SetContentSize() which allows
// to determine if the panel will recalculate its layout and dispatch event.
// It is normally used by layout managers when setting the panel content size
// to avoid another invokation of the layout manager.
func (p *Panel) setContentSize(width, height float64, dispatch bool) {

	// Calculates the new desired external width and height
	eWidth := width +
		p.paddingSizes.Left + p.paddingSizes.Right +
		p.borderSizes.Left + p.borderSizes.Right +
		p.marginSizes.Left + p.marginSizes.Right
	eHeight := height +
		p.paddingSizes.Top + p.paddingSizes.Bottom +
		p.borderSizes.Top + p.borderSizes.Bottom +
		p.marginSizes.Top + p.marginSizes.Bottom
	p.resize(eWidth, eHeight, dispatch)
}

// setParentOf is used by Add and AddAt.
// It verifies that the node is not being added to itself and sets the parent pointer of the specified node.
// If the specified node had a parent, the specified node is removed from the original parent's list of children.
// It does not add the specified node to the list of children.
func (p *Panel) setParentOf(ichild IPanel) {

	child := ichild.GetPanel()
	if p == child {
		panic("Panel.{Add,AddAt}: object can't be added as a child of itself")
	}
	// If the specified child already has a parent,
	// remove it from the original parent's list of children
	if child.parent != nil {
		child.parent.GetPanel().Remove(ichild)
	}
	child.parent = p
}

// updateBounds calculates the absolute coordinates of this panel.
// It assumes that updateBounds() was already called for this panel's parent.
func (p *Panel) updateBounds() {

	//fmt.Printf("updateBounds:%p\n", p)
	if p.parent == nil {
		p.xabs = p.x
		p.yabs = p.y
		p.xabsMin = -100000
		p.xabsMax = 100000
		p.yabsMin = -100000
		p.yabsMax = 100000

	} else {

		// Sets the absolute position coordinates for this panel
		pp := p.parent.GetPanel()
		p.xabs = p.x + pp.xabs
		p.yabs = p.y + pp.yabs
		if p.bounded {
			p.xabs = p.x + pp.xabs + pp.marginSizes.Left + pp.borderSizes.Left + pp.paddingSizes.Left
			p.yabs = p.y + pp.yabs + pp.marginSizes.Top + pp.borderSizes.Top + pp.paddingSizes.Top
		}

		// Initialize maximum and minimum x,y coordinates for this panel
		p.xabsMin = p.xabs
		p.yabsMin = p.yabs
		p.xabsMax = p.xabs + p.width
		p.yabsMax = p.yabs + p.height

		// Calculates the parent content area minimum and maximum absolute coordinates in pixels
		if p.bounded {
			pxmin := pp.xabs + pp.marginSizes.Left + pp.borderSizes.Left + pp.paddingSizes.Left
			if pxmin < pp.xabsMin {
				pxmin = pp.xabsMin
			}
			pymin := pp.yabs + pp.marginSizes.Top + pp.borderSizes.Top + pp.paddingSizes.Top
			if pymin < pp.yabsMin {
				pymin = pp.yabsMin
			}

			pxmax := pp.xabs + pp.width - (pp.marginSizes.Right + pp.borderSizes.Right + pp.paddingSizes.Right)
			if pxmax > pp.xabsMax {
				pxmax = pp.xabsMax
			}
			pymax := pp.yabs + pp.height - (pp.marginSizes.Bottom + pp.borderSizes.Bottom + pp.paddingSizes.Bottom)
			if pymax > pp.yabsMax {
				pymax = pp.yabsMax
			}

			// Update this panel minimum x and y coordinates.
			if p.xabsMin < pxmin {
				p.xabsMin = pxmin
			}
			if p.yabsMin < pymin {
				p.yabsMin = pymin
			}
			// Update this panel maximum x and y coordinates.
			if p.xabsMax > pxmax {
				p.xabsMax = pxmax
			}
			if p.yabsMax > pymax {
				p.yabsMax = pymax
			}
		}
	}
}
